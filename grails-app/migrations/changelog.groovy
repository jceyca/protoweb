databaseChangeLog = {

    changeSet(author: "7102 (generated)", id: "1590111790377-1") {
        createSequence(sequenceName: "hibernate_sequence")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-2") {
        createTable(tableName: "COMPANIEROS") {
            column(name: "ID", type: "VARCHAR(255)")

            column(name: "apmaterno", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "appaterno", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "area", type: "VARCHAR(255)")

            column(name: "domicilio", type: "VARCHAR(255)")

            column(name: "empresa", type: "VARCHAR(255)")

            column(name: "enabled", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "id_autor", type: "BIGINT")

            column(name: "nombre", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "rubro", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "telefono", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-3") {
        createTable(tableName: "FAMILIARES") {
            column(name: "ID", type: "VARCHAR(255)")

            column(name: "apmaterno", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "appaterno", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "domicilio", type: "VARCHAR(255)")

            column(name: "enabled", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "id_autor", type: "BIGINT")

            column(name: "nombre", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "telefono", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "tipo", type: "VARCHAR(11)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-4") {
        createTable(tableName: "ROLELG") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "ROLELGPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "authority", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "id_autor", type: "BIGINT")

            column(name: "last_updated", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "nombre", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-5") {
        createTable(tableName: "SESION") {
            column(name: "ID", type: "INT") {
                constraints(nullable: "false")
            }

            column(name: "FINGRESO", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "SESION_NO", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "USER_ID", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-6") {
        createTable(tableName: "USERLG") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "USERLGPK")
            }

            column(name: "account_expired", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "account_locked", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "date_created", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "enabled", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "expire_date", type: "timestamp")

            column(name: "id_autor_id", type: "BIGINT")

            column(name: "last_updated", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "nombre", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "password_expired", type: "BOOLEAN") {
                constraints(nullable: "false")
            }

            column(name: "password_plain", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-7") {
        createTable(tableName: "userlgrolelg") {
            column(name: "userlg_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "rolelg_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-8") {
        addPrimaryKey(columnNames: "ID", constraintName: "COMPANIEROSPK", tableName: "COMPANIEROS")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-9") {
        addPrimaryKey(columnNames: "ID", constraintName: "FAMILIARESPK", tableName: "FAMILIARES")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-10") {
        addPrimaryKey(columnNames: "ID", constraintName: "SESIONPK", tableName: "SESION")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-11") {
        addPrimaryKey(columnNames: "userlg_id, rolelg_id", constraintName: "userlgrolelgPK", tableName: "userlgrolelg")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-12") {
        addUniqueConstraint(columnNames: "username", constraintName: "UC_USERLGUSERNAME_COL", tableName: "USERLG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-13") {
        addForeignKeyConstraint(baseColumnNames: "id_autor", baseTableName: "FAMILIARES", constraintName: "FK_54mtj81d4l4h1k3nasrt5bv81", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "USERLG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-14") {
        addForeignKeyConstraint(baseColumnNames: "id_autor", baseTableName: "ROLELG", constraintName: "FK_7cqfvyadp3fwwcayglbipxk6i", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "USERLG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-15") {
        addForeignKeyConstraint(baseColumnNames: "id_autor_id", baseTableName: "USERLG", constraintName: "FK_g9w35w61gxsotfaeb0vgs5joc", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "USERLG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-16") {
        addForeignKeyConstraint(baseColumnNames: "rolelg_id", baseTableName: "userlgrolelg", constraintName: "FK_gnaivvcklboxqpvmihi0iphuf", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "ROLELG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-17") {
        addForeignKeyConstraint(baseColumnNames: "userlg_id", baseTableName: "userlgrolelg", constraintName: "FK_n3iis4i4tmwk1yhplprpdiwmn", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "USERLG")
    }

    changeSet(author: "7102 (generated)", id: "1590111790377-18") {
        addForeignKeyConstraint(baseColumnNames: "id_autor", baseTableName: "COMPANIEROS", constraintName: "FK_ovou1xm0v81ychri871ruxvbk", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "USERLG")
    }
}

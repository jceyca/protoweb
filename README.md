# README #

protoWeb

El presente proyecto es para fines demostrativos de conocimiento en desarrollo de portales Web basados en tecnologías Java (bajo Framework Grails). El presente proyecto se desarrollo usando las siguientes tecnologías:

- [x] Hibernate 5
- [x] Spring Framework 4
- [x] Spring Boot 1.5
- [x] Gradle 3.5
- [x] Spock 1

# DETALLANDO LA APLICACION #

![Test Image 1](ss1.png)

La aplicación Web tiene mecanismos de validación de acceso usando Spring Security, cuyos usuarios son gestionados desde un CRUD de usuarios accesibles para los roles del Administrador y Programador.

Se disponen de los siguientes usuarios para accesar a la aplicación:

> usario: admin  y la contraseña: admin
> usario: programador  y la contraseña: C3yc@2208

```
URL de acceso: http://198.71.60.28/prototipo/

       (Este es un servidor para pruebas de concepto que uso)

donde las opciones del menú incluyen:
    Inicio							             pagina de inicio de la aplicación (se puede poner un dashboard 
                                                 o mensaje de bienvenida)
    Seguridad                                    CRUD para la gestión de usuarios y asignación de roles, se puede
                                                 restablecer contraseñas, activar o desactivar cuentas y eliminarlas.
    Cerrar Sesión                                cierra el acceso válido a la aplicación web, regresando a la pagina
                                                 de autentificación
```

El script para la generación de la base de datos se ubica en grails-app/migrations/changelog.groovy que al momento de ser deployado el proyecto en el servidor Apache Tomcat genera todas las tablas de la base de datos y carga los datos iniciales en la misma.

    <script language="javascript">
        $(document).ready(function(){
			$('input[type="text"],input[type="password"],input[type="email"],textarea').alphanum({
				allowPlus           : true, // Allow the + sign
				allowMinus          : true,  // Allow the - sign
				allowThouSep        : true,  // Allow the thousands separator, default is the comma eg 12,000
				allowDecSep         : true,
				allow				: '@,:.#-_()¿?!¡*$%/+=&;{}',
				disallow 			: '∞ª¨☺☻♥♦♣♠•◘○◙♂♀♪♫☼►◄↕‼¶§▬↨↑↓→←∟↔▲▼"^`<>|~⌂ÇçæÆ¢£¥₧ƒ⌐¬½¼«»░▒▓│┤╡╢╖╕╣║╗╝╜╛┐└┴┬├─┼╞╟╚╔╩╦╠═╬╧╨╤╥╙╘╒╓╫╪┘┌█▄▌▐▀αßΓπΣσµτΦΘΩδ∞φε∩≡±≥≤⌠⌡÷≈°∙·√ⁿ²■ '
			});
		});
    </script>
    <content tag="nav">
    <sec:ifLoggedIn>
		<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_PROGRAMMER,ROLE_OPERATOR,ROLE_EXECUTIVE'>
			<li class="dropdown">
				<a href="${request.contextPath}/">
				<i class="fa fa-home fa" aria-hidden="true"></i></br>
				Inicio
				</a>
			</li>
		</sec:ifAnyGranted> 
		<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_PROGRAMMER'>
			<li class="dropdown">
				<a href="${request.contextPath}/userLG/index">
				<i class="fa fa-lock" aria-hidden="true"></i></br>
				Seguridad
				</a>
			</li>
			<li class="dropdown">
				<a href="${request.contextPath}/logout">
				<i class="fa fa-users" aria-hidden="true"></i></br>
				Cerrar sesión
				</a>
			</li>
		</sec:ifAnyGranted>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
    
    </sec:ifNotLoggedIn>
    </content>

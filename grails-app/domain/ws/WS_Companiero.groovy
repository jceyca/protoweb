package ws

import grails.rest.*

@Resource(uri='/WS_Companiero', formats=['json', 'xml'])
class WS_Companiero {

	String	id
    String	nombre
    String	apmaterno
    String	appaterno
    String	telefono
    String	domicilio
    String	empresa
    String	area
    String	rubro
    UserLG	id_Autor
    
	boolean enabled = Boolean.TRUE
	
    static constraints = {
		id				(nullable:true)
		nombre			(nullable:false)
		apmaterno		(nullable:false)
		appaterno		(nullable:false)
		telefono		(nullable:false)
		domicilio		(nullable:true, widget: 'textarea')
		empresa			(nullable:true)
        area			(nullable:true)
        rubro			(nullable:false)
        
        id_Autor		(nullable:true, widget:'display')
    }

    static mapping = {
		sort nombre: "asc"
		table 'COMPANIEROS'
		version false
		id column: "ID", generator : 'uuid'
        id_Autor column:'id_autor'
    }

    String toString() {
        return nombre 
    }
}
